import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const message = ref("");
  const isShow = ref(false);

  function showError(text: string) {
    message.value = text;
    isShow.value = true;
  }

  return { isShow, message, showError };
});
