export default interface Product {
  id?: number;

  name: string;

  price: number;

  createdAt?: Date;

  updateAt?: Date;

  deleteAt?: Date;
}
